﻿namespace usacXForm
{
    partial class tabContent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.entradaText = new FastColoredTextBoxNS.FastColoredTextBox();
            this.consolaText = new FastColoredTextBoxNS.FastColoredTextBox();
            this.SuspendLayout();
            // 
            // entradaText
            // 
            this.entradaText.AutoScrollMinSize = new System.Drawing.Size(25, 15);
            this.entradaText.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.entradaText.Location = new System.Drawing.Point(8, 8);
            this.entradaText.Name = "entradaText";
            this.entradaText.Size = new System.Drawing.Size(1066, 364);
            this.entradaText.TabIndex = 1;
            // 
            // consolaText
            // 
            this.consolaText.AutoScrollMinSize = new System.Drawing.Size(25, 15);
            this.consolaText.BackColor = System.Drawing.Color.Black;
            this.consolaText.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.consolaText.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.consolaText.Location = new System.Drawing.Point(8, 393);
            this.consolaText.Name = "consolaText";
            this.consolaText.Size = new System.Drawing.Size(1066, 206);
            this.consolaText.TabIndex = 2;
            // 
            // tabContent
            // 
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1134, 611);
            this.Controls.Add(this.consolaText);
            this.Controls.Add(this.entradaText);
            this.Name = "tabContent";
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox consolaTextBox;
        private System.Windows.Forms.RichTextBox entradaTextBox;
        private FastColoredTextBoxNS.FastColoredTextBox entradaText;
        private FastColoredTextBoxNS.FastColoredTextBox consolaText;
    }
}