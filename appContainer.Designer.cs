﻿namespace usacXForm
{
    partial class appContainer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarComoMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formularioMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevaPestañaMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarPestañaMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualTécnicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualDeUsuarioMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grámaticaUtilizadaMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAnalizar = new System.Windows.Forms.Button();
            this.cargarArchivoButton = new System.Windows.Forms.Button();
            this.generarFormButton = new System.Windows.Forms.Button();
            this.verFormsButton = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoMenuItem,
            this.formularioMenuItem,
            this.manualMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip";
            // 
            // archivoMenuItem
            // 
            this.archivoMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirMenuItem,
            this.guardarMenuItem,
            this.guardarComoMenuItem});
            this.archivoMenuItem.Name = "archivoMenuItem";
            this.archivoMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoMenuItem.Text = "Archivo";
            // 
            // abrirMenuItem
            // 
            this.abrirMenuItem.Name = "abrirMenuItem";
            this.abrirMenuItem.Size = new System.Drawing.Size(152, 22);
            this.abrirMenuItem.Text = "Abrir";
            // 
            // guardarMenuItem
            // 
            this.guardarMenuItem.Name = "guardarMenuItem";
            this.guardarMenuItem.Size = new System.Drawing.Size(152, 22);
            this.guardarMenuItem.Text = "Guardar";
            // 
            // guardarComoMenuItem
            // 
            this.guardarComoMenuItem.Name = "guardarComoMenuItem";
            this.guardarComoMenuItem.Size = new System.Drawing.Size(152, 22);
            this.guardarComoMenuItem.Text = "Guardar Como";
            // 
            // formularioMenuItem
            // 
            this.formularioMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevaPestañaMenuItem,
            this.cerrarPestañaMenuItem});
            this.formularioMenuItem.Name = "formularioMenuItem";
            this.formularioMenuItem.Size = new System.Drawing.Size(77, 20);
            this.formularioMenuItem.Text = "Formulario";
            // 
            // nuevaPestañaMenuItem
            // 
            this.nuevaPestañaMenuItem.Name = "nuevaPestañaMenuItem";
            this.nuevaPestañaMenuItem.Size = new System.Drawing.Size(152, 22);
            this.nuevaPestañaMenuItem.Text = "Nueva Pestaña";
            // 
            // cerrarPestañaMenuItem
            // 
            this.cerrarPestañaMenuItem.Name = "cerrarPestañaMenuItem";
            this.cerrarPestañaMenuItem.Size = new System.Drawing.Size(152, 22);
            this.cerrarPestañaMenuItem.Text = "Cerrar Pestaña";
            // 
            // manualMenuItem
            // 
            this.manualMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manualTécnicoToolStripMenuItem,
            this.manualDeUsuarioMenuItem,
            this.grámaticaUtilizadaMenuItem});
            this.manualMenuItem.Name = "manualMenuItem";
            this.manualMenuItem.Size = new System.Drawing.Size(59, 20);
            this.manualMenuItem.Text = "Manual";
            // 
            // manualTécnicoToolStripMenuItem
            // 
            this.manualTécnicoToolStripMenuItem.Name = "manualTécnicoToolStripMenuItem";
            this.manualTécnicoToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.manualTécnicoToolStripMenuItem.Text = "Manual Técnico";
            // 
            // manualDeUsuarioMenuItem
            // 
            this.manualDeUsuarioMenuItem.Name = "manualDeUsuarioMenuItem";
            this.manualDeUsuarioMenuItem.Size = new System.Drawing.Size(176, 22);
            this.manualDeUsuarioMenuItem.Text = "Manual de Usuario";
            // 
            // grámaticaUtilizadaMenuItem
            // 
            this.grámaticaUtilizadaMenuItem.Name = "grámaticaUtilizadaMenuItem";
            this.grámaticaUtilizadaMenuItem.Size = new System.Drawing.Size(176, 22);
            this.grámaticaUtilizadaMenuItem.Text = "Grámatica Utilizada";
            // 
            // btnAnalizar
            // 
            this.btnAnalizar.Location = new System.Drawing.Point(12, 41);
            this.btnAnalizar.Name = "btnAnalizar";
            this.btnAnalizar.Size = new System.Drawing.Size(89, 22);
            this.btnAnalizar.TabIndex = 7;
            this.btnAnalizar.Text = "Analizar";
            this.btnAnalizar.UseVisualStyleBackColor = true;
            // 
            // cargarArchivoButton
            // 
            this.cargarArchivoButton.Location = new System.Drawing.Point(143, 39);
            this.cargarArchivoButton.Name = "cargarArchivoButton";
            this.cargarArchivoButton.Size = new System.Drawing.Size(75, 23);
            this.cargarArchivoButton.TabIndex = 9;
            this.cargarArchivoButton.Text = "Cargar Archivo";
            this.cargarArchivoButton.UseVisualStyleBackColor = true;
            // 
            // generarFormButton
            // 
            this.generarFormButton.Location = new System.Drawing.Point(243, 39);
            this.generarFormButton.Name = "generarFormButton";
            this.generarFormButton.Size = new System.Drawing.Size(98, 23);
            this.generarFormButton.TabIndex = 10;
            this.generarFormButton.Text = "Generar Form";
            this.generarFormButton.UseVisualStyleBackColor = true;
            // 
            // verFormsButton
            // 
            this.verFormsButton.Location = new System.Drawing.Point(373, 38);
            this.verFormsButton.Name = "verFormsButton";
            this.verFormsButton.Size = new System.Drawing.Size(75, 23);
            this.verFormsButton.TabIndex = 11;
            this.verFormsButton.Text = "Ver Forms";
            this.verFormsButton.UseVisualStyleBackColor = true;
            // 
            // appContainer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.verFormsButton);
            this.Controls.Add(this.generarFormButton);
            this.Controls.Add(this.cargarArchivoButton);
            this.Controls.Add(this.btnAnalizar);
            this.Controls.Add(this.menuStrip1);
            this.Name = "appContainer";
            this.Text = "appContainer";
            this.Load += new System.EventHandler(this.appContainer_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarComoMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formularioMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevaPestañaMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarPestañaMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manualMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manualTécnicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manualDeUsuarioMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grámaticaUtilizadaMenuItem;
        private System.Windows.Forms.Button btnAnalizar;
        private System.Windows.Forms.Button cargarArchivoButton;
        private System.Windows.Forms.Button generarFormButton;
        private System.Windows.Forms.Button verFormsButton;
    }
}