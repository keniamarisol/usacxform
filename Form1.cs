﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using usacXForm.analizadores;
using EasyTabs;
using System.IO;
using Irony.Ast;
using Irony.Parsing;
using usacXForm.graficar;
using FastColoredTextBoxNS;

namespace usacXForm
{
    public partial class Form1 : Form
    {

        tabContent panel;
        FastColoredTextBox entradaTxt;
        FastColoredTextBox consolaTxt;
        public Form1()
        {
            InitializeComponent();
            tabContent frm = new tabContent();
            frm.TopLevel = false;
            frm.Visible = true;
            frm.FormBorderStyle = FormBorderStyle.None;
            frm.Dock = DockStyle.Fill;
            tabControl.TabPages[0].Controls.Add(frm);
        }


        private void btnAnalizar_Click(object sender, EventArgs e)
        {

            panel = (tabContent)tabControl.SelectedTab.Controls["tabContent"];
            entradaTxt = (FastColoredTextBox)panel.Controls["entradaText"];
            consolaTxt = (FastColoredTextBox)panel.Controls["consolaText"];

            gramatica gram = new gramatica();
            LanguageData lenguaje = new LanguageData(gram);
            Parser parser = new Parser(lenguaje);

            try
            {
                ParseTree arbol = parser.Parse(entradaTxt.Text);
                ParseTreeNode raiz = arbol.Root;

                if (raiz == null || arbol.ParserMessages.Count > 0 || arbol.HasErrors())
                {
                   // consolaTxt.Text += "Error sintactico en la cadena de entrada\r\n";
                    foreach (var item in arbol.ParserMessages)
                    {
                        consolaTxt.Text += "Linea:"+ (Convert.ToInt32(item.Location.Line)+1).ToString() + ", Colum:"+item.Location.Column+ ", Msg:" + item.Message + "\r\n";
                    }


                }
                else
                {
                    interprete interp = new interprete();
                    generarImagen(raiz);
                  //  Graficador g = new Graficador();
                  //  g.graficar(arbol);
                  //  interp.ejecutar(raiz);
                    consolaTxt.Text += "Cadena de entrada sintacticamente correcta\r\n";
                    consolaTxt.Text += interp.salida.ToString() + "\r\n";
                    consolaTxt.Text += interp.errores.ToString() + "\r\n";
                }
            }
            catch (Exception eX)
            {
                MessageBox.Show(eX.ToString()); 
            }


        }

        private static void generarImagen(ParseTreeNode raiz)
        {
            String graf = graficar.graficar.getdot(raiz);
            WINGRAPHVIZLib.DOT dot = new WINGRAPHVIZLib.DOT();
            WINGRAPHVIZLib.BinaryImage img = dot.ToPNG(graf);

            img.Save("AST.png");
        }

        private void abrirMenuItem_Click(object sender, EventArgs e)
        {
            panel = (tabContent)tabControl.SelectedTab.Controls["tabContent"];
            entradaTxt = (FastColoredTextBox)panel.Controls["entradaText"];
            consolaTxt = (FastColoredTextBox)panel.Controls["consolaText"];
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = @"C:\";
            openFileDialog1.Title = "Seleccionar archivo de entrada";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.DefaultExt = "txt";
            openFileDialog1.Filter = "Xform files (*.xform)|*.xform|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.ReadOnlyChecked = true;
            openFileDialog1.ShowReadOnly = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                entradaTxt.Text = File.ReadAllText(openFileDialog1.FileName);
            }
        }
    }
}
