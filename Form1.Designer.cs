﻿namespace usacXForm
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAnalizar = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarComoMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.formularioMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevaPestañaMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarPestañaMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualTécnicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualDeUsuarioMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grámaticaUtilizadaMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cargarArchivoButton = new System.Windows.Forms.Button();
            this.generarFormButton = new System.Windows.Forms.Button();
            this.verFormsButton = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.menuStrip1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAnalizar
            // 
            this.btnAnalizar.Location = new System.Drawing.Point(12, 28);
            this.btnAnalizar.Name = "btnAnalizar";
            this.btnAnalizar.Size = new System.Drawing.Size(89, 22);
            this.btnAnalizar.TabIndex = 1;
            this.btnAnalizar.Text = "Analizar";
            this.btnAnalizar.UseVisualStyleBackColor = true;
            this.btnAnalizar.Click += new System.EventHandler(this.btnAnalizar_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoMenuItem,
            this.formularioMenuItem,
            this.manualMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1184, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip";
            // 
            // archivoMenuItem
            // 
            this.archivoMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.abrirMenuItem,
            this.guardarMenuItem,
            this.guardarComoMenuItem});
            this.archivoMenuItem.Name = "archivoMenuItem";
            this.archivoMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoMenuItem.Text = "Archivo";
            // 
            // abrirMenuItem
            // 
            this.abrirMenuItem.Name = "abrirMenuItem";
            this.abrirMenuItem.Size = new System.Drawing.Size(180, 22);
            this.abrirMenuItem.Text = "Abrir";
            this.abrirMenuItem.Click += new System.EventHandler(this.abrirMenuItem_Click);
            // 
            // guardarMenuItem
            // 
            this.guardarMenuItem.Name = "guardarMenuItem";
            this.guardarMenuItem.Size = new System.Drawing.Size(152, 22);
            this.guardarMenuItem.Text = "Guardar";
            // 
            // guardarComoMenuItem
            // 
            this.guardarComoMenuItem.Name = "guardarComoMenuItem";
            this.guardarComoMenuItem.Size = new System.Drawing.Size(152, 22);
            this.guardarComoMenuItem.Text = "Guardar Como";
            // 
            // formularioMenuItem
            // 
            this.formularioMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevaPestañaMenuItem,
            this.cerrarPestañaMenuItem});
            this.formularioMenuItem.Name = "formularioMenuItem";
            this.formularioMenuItem.Size = new System.Drawing.Size(77, 20);
            this.formularioMenuItem.Text = "Formulario";
            // 
            // nuevaPestañaMenuItem
            // 
            this.nuevaPestañaMenuItem.Name = "nuevaPestañaMenuItem";
            this.nuevaPestañaMenuItem.Size = new System.Drawing.Size(152, 22);
            this.nuevaPestañaMenuItem.Text = "Nueva Pestaña";
            // 
            // cerrarPestañaMenuItem
            // 
            this.cerrarPestañaMenuItem.Name = "cerrarPestañaMenuItem";
            this.cerrarPestañaMenuItem.Size = new System.Drawing.Size(152, 22);
            this.cerrarPestañaMenuItem.Text = "Cerrar Pestaña";
            // 
            // manualMenuItem
            // 
            this.manualMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manualTécnicoToolStripMenuItem,
            this.manualDeUsuarioMenuItem,
            this.grámaticaUtilizadaMenuItem});
            this.manualMenuItem.Name = "manualMenuItem";
            this.manualMenuItem.Size = new System.Drawing.Size(59, 20);
            this.manualMenuItem.Text = "Manual";
            // 
            // manualTécnicoToolStripMenuItem
            // 
            this.manualTécnicoToolStripMenuItem.Name = "manualTécnicoToolStripMenuItem";
            this.manualTécnicoToolStripMenuItem.Size = new System.Drawing.Size(176, 22);
            this.manualTécnicoToolStripMenuItem.Text = "Manual Técnico";
            // 
            // manualDeUsuarioMenuItem
            // 
            this.manualDeUsuarioMenuItem.Name = "manualDeUsuarioMenuItem";
            this.manualDeUsuarioMenuItem.Size = new System.Drawing.Size(176, 22);
            this.manualDeUsuarioMenuItem.Text = "Manual de Usuario";
            // 
            // grámaticaUtilizadaMenuItem
            // 
            this.grámaticaUtilizadaMenuItem.Name = "grámaticaUtilizadaMenuItem";
            this.grámaticaUtilizadaMenuItem.Size = new System.Drawing.Size(176, 22);
            this.grámaticaUtilizadaMenuItem.Text = "Grámatica Utilizada";
            // 
            // cargarArchivoButton
            // 
            this.cargarArchivoButton.Location = new System.Drawing.Point(124, 28);
            this.cargarArchivoButton.Name = "cargarArchivoButton";
            this.cargarArchivoButton.Size = new System.Drawing.Size(75, 23);
            this.cargarArchivoButton.TabIndex = 4;
            this.cargarArchivoButton.Text = "Cargar Archivo";
            this.cargarArchivoButton.UseVisualStyleBackColor = true;
            // 
            // generarFormButton
            // 
            this.generarFormButton.Location = new System.Drawing.Point(222, 28);
            this.generarFormButton.Name = "generarFormButton";
            this.generarFormButton.Size = new System.Drawing.Size(98, 23);
            this.generarFormButton.TabIndex = 5;
            this.generarFormButton.Text = "Generar Form";
            this.generarFormButton.UseVisualStyleBackColor = true;
            // 
            // verFormsButton
            // 
            this.verFormsButton.Location = new System.Drawing.Point(343, 28);
            this.verFormsButton.Name = "verFormsButton";
            this.verFormsButton.Size = new System.Drawing.Size(75, 23);
            this.verFormsButton.TabIndex = 6;
            this.verFormsButton.Text = "Ver Forms";
            this.verFormsButton.UseVisualStyleBackColor = true;
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Location = new System.Drawing.Point(12, 56);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1150, 620);
            this.tabControl.TabIndex = 7;
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1142, 594);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1184, 711);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.verFormsButton);
            this.Controls.Add(this.generarFormButton);
            this.Controls.Add(this.cargarArchivoButton);
            this.Controls.Add(this.btnAnalizar);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Uscar XForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnAnalizar;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarComoMenuItem;
        private System.Windows.Forms.ToolStripMenuItem formularioMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevaPestañaMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarPestañaMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manualMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manualTécnicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem manualDeUsuarioMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grámaticaUtilizadaMenuItem;
        private System.Windows.Forms.Button cargarArchivoButton;
        private System.Windows.Forms.Button generarFormButton;
        private System.Windows.Forms.Button verFormsButton;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPage1;
    }
}

