﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Irony.Ast;
using Irony.Parsing;
using usacXForm.graficar;


namespace usacXForm.analizadores
{
    class interprete
    {

        #region "Atributos"
        public StringBuilder salida = new StringBuilder();
        public StringBuilder errores = new StringBuilder();
        public Dictionary<string, clase> clases = new Dictionary<string, clase>();
        public Boolean es_heredada = false;
        #endregion

        #region "Interprete"
        public interprete()
        {
           
        }
        #endregion

        public void ejecutar(ParseTreeNode raiz)
        {
            switch (raiz.Term.Name)
            {
                case "PROGRAMA":
                    foreach (ParseTreeNode a in raiz.ChildNodes)
                        ejecutar(a);
                    break;
                case "CLASE":
                    clase obj = new clase();
                       clase(raiz, obj);
                    break;
                case "IMPORTAR":
                   
                    break;            
            }
        }

        private void importar(ParseTreeNode raiz)
        {
            switch (raiz.Term.Name)
            {
                case "ID":
                   
                    break;
            }

        }

        
        private void clase(ParseTreeNode raiz, clase obj)
        {
            ArrayList hereda = new ArrayList();
            foreach (ParseTreeNode a in raiz.ChildNodes) { 
                switch (a.Term.Name)
                {
                    case "ID":
                        obj.nombre = a.ChildNodes[0].Term.Name;
                        break;
                    case "VISIBILIDAD":
                        obj.visibilidad = a.ChildNodes[0].Term.Name;
                        break;
                    case "HEREDA":
                        hereda.Add(a.ChildNodes[0].Term.Name);
                        break;
                    case "CLASE_SENTENCIAS":
                       claseSentencias(a, obj);
                        break;
                }
                obj.hereda = hereda;
        }

        }

        private void claseSentencias(ParseTreeNode raiz, clase obj)
        {
            metodo objMetodo;
            Variable objVar;
            foreach (ParseTreeNode a in raiz.ChildNodes)
            {
                switch (a.Term.Name)
                {
                    case "PRINCIPAL":
                        objMetodo = new metodo();
                        metodo(a, objMetodo);
                        obj.principal.Add(objMetodo.nombre, objMetodo);
                        break;
                    case "CONSTRUCTOR":
                        objMetodo = new metodo();
                        metodo(a, objMetodo);
                        obj.constructores.Add(objMetodo.nombre, objMetodo);
                        break;
                    case "DECLARAR_GLOBAL":
                        objVar = new Variable();

                        break;
                    case "INSTANCIAR_GLOBAL":

                        break;
                    case "DECLARAR_ASIG_GLOBAL":

                        break;
                    case "ARREGLO_GLOBAL":

                        break;
                    case "OVERRIDE":
                        objMetodo = new metodo();
                        metodo(a, objMetodo);
                        obj.metodos.Add(objMetodo.nombre, objMetodo);
                        break;
                    case "METODO":
                        objMetodo = new metodo();
                        metodo(a, objMetodo);
                        obj.metodos.Add(objMetodo.nombre, objMetodo);
                        break;
                    case "FORMULARIO":
                        objMetodo = new metodo();
                        metodo(a, objMetodo);
                        obj.metodos.Add(objMetodo.nombre, objMetodo);
                        break;
                    case "PREGUNTA":
                        objMetodo = new metodo();
                        metodo(a, objMetodo);
                        obj.metodos.Add(objMetodo.nombre, objMetodo);
                        break;
                    case "GRUPO":
                        objMetodo = new metodo();
                        metodo(a, objMetodo);
                        obj.metodos.Add(objMetodo.nombre, objMetodo);
                        break;
                }
            }
            
        }



        private void metodo(ParseTreeNode raiz, metodo obj)
        {
            foreach (ParseTreeNode a in raiz.ChildNodes)
            {
                switch (a.Term.Name)
                {
                    case "VISIBILIDAD":
                        obj.visibilidad = a.ChildNodes[0].Term.Name; ;
                        break;
                    case "TIPO_M":
                        obj.tipo = a.ChildNodes[0].Term.Name;
                        break;
                    case "ID":
                        obj.nombre = a.ChildNodes[0].Term.Name; 
                        break;
                    case "SENTENCIAS":
                        obj.sentencia = a;
                        break;
                    case "PARAMETRO_LIST":
                        obj.parametros = a;
                        break;
                }
            }
        }

        private void sentencias(ParseTreeNode raiz)
        {

            foreach (ParseTreeNode a in raiz.ChildNodes)
            {
                switch (a.Term.Name)
                {
                    case "IMPRIMIR":
                        imprimir(a);
                        break;
                }
            }
        }

        private void imprimir(ParseTreeNode nodo)
        {
            String sms = evaluarExp(nodo.ChildNodes[0]).ToString();
            this.salida.Append(sms.ToString() + "\n");
        }

        private Object evaluarExp(ParseTreeNode nodo)
        {


            if (nodo.ChildNodes.Count == 3)
            {
                String operador = nodo.ChildNodes[1].Term.Name;
                switch (operador)
                {
                    case "||": return evaluarOr(nodo.ChildNodes[0], nodo.ChildNodes[2]);
                    case "&&": return evaluarAnd(nodo.ChildNodes[0], nodo.ChildNodes[2]);
                    case "==": return evaluarIgual(nodo.ChildNodes[0], nodo.ChildNodes[2]);
                    case "!=": return evaluarDiferente(nodo.ChildNodes[0], nodo.ChildNodes[2]);
                    case ">=": return evaluarMayorIgual(nodo.ChildNodes[0], nodo.ChildNodes[2]);
                    case ">": return evaluarMayor(nodo.ChildNodes[0], nodo.ChildNodes[2]);
                    case "<=": return evaluarMenorigual(nodo.ChildNodes[0], nodo.ChildNodes[2]);
                    case "<": return evaluarMenor(nodo.ChildNodes[0], nodo.ChildNodes[2]);
                    case "+": return evaluarSuma(nodo.ChildNodes[0], nodo.ChildNodes[2]);
                    case "-": return evaluarResta(nodo.ChildNodes[0], nodo.ChildNodes[2]);
                    case "*": return evaluarPor(nodo.ChildNodes[0], nodo.ChildNodes[2]);
                    case "/": return evaluarDividir(nodo.ChildNodes[0], nodo.ChildNodes[2]);
                    case "%": return evaluarMod(nodo.ChildNodes[0], nodo.ChildNodes[2]);
                    case "^": return evaluarPot(nodo.ChildNodes[0], nodo.ChildNodes[2]);
                    default: break;
                }


                operador = nodo.ChildNodes[0].Term.Name;
                switch (operador)
                {
                    case "pow": return evaluarPow(nodo.ChildNodes[1], nodo.ChildNodes[2]);
                    default: break;
                }
            }




            if (nodo.ChildNodes.Count == 2)
            {
                if (nodo.ChildNodes[0].Term.Name.Equals("!"))
                    return evaluarNot(nodo.ChildNodes[1]);
                else if (nodo.ChildNodes[0].Term.Name.ToLower().Equals("log"))
                    return evaluarLog(nodo.ChildNodes[1]);
                else if (nodo.ChildNodes[0].Term.Name.ToLower().Equals("log10"))
                    return evaluarLog10(nodo.ChildNodes[1]);
                else if (nodo.ChildNodes[0].Term.Name.ToLower().Equals("abs"))
                    return evaluarAbs(nodo.ChildNodes[1]);
                else if (nodo.ChildNodes[0].Term.Name.ToLower().Equals("sin"))
                    return evaluarSin(nodo.ChildNodes[1]);
                else if (nodo.ChildNodes[0].Term.Name.ToLower().Equals("cos"))
                    return evaluarCos(nodo.ChildNodes[1]);
                else if (nodo.ChildNodes[0].Term.Name.ToLower().Equals("tan"))
                    return evaluarTan(nodo.ChildNodes[1]);
                else if (nodo.ChildNodes[0].Term.Name.ToLower().Equals("sqrt"))
                    return evaluarSqrt(nodo.ChildNodes[1]);

            }




            if (nodo.ChildNodes.Count == 1)
            {

                if (nodo.ChildNodes[0].Term.Name.ToLower().Equals("pi"))
                    return evaluarPi();

                String termino = nodo.ChildNodes[0].Term.Name;
                switch (termino.ToLower())
                {

                    case "EXP": return evaluarExp(nodo.ChildNodes[0]);
                    case "id": return evaluarId(nodo.ChildNodes[0]);
                    case "numero": return evaluarNumero(nodo.ChildNodes[0]);
                    case "tstring": return nodo.ChildNodes[0].Token.Text.Replace("\"", "");
                    case "tchar": return nodo.ChildNodes[0].Token.Text.Replace("'", "");
                    case "verdadero": return true;
                    case "falso": return false;
                    default: break;
                }
            }

            return 1;
        }

        private Object evaluarPow(ParseTreeNode izq, ParseTreeNode der)
        {
            Object val1 = evaluarExp(izq);
            Object val2 = evaluarExp(der);
            int tipo1 = getTipo(val1);
            int tipo2 = getTipo(val2);

            if (tipo1 == 2 || tipo2 == 2)
                return Math.Pow(Convert.ToDouble(val1), Convert.ToDouble(val2));
            else if (tipo1 == 1 && tipo2 == 1)
                return Math.Pow(Convert.ToDouble(val1), Convert.ToDouble(val2));

            this.errores.Append("Función Pow: Tipo de dato inválido, esperaba un valor de tipo númerico. \r\n");
            return 1;
        }

        private Object evaluarLog(ParseTreeNode der)
        {
            Object val1 = evaluarExp(der);
            int tipo1 = getTipo(val1);

            if (tipo1 == 2 || tipo1 == 1)
                return Math.Log(Convert.ToDouble(val1));


            this.errores.Append("Función Log: Tipo de dato inválido, esperaba un valor de tipo númerico. \r\n");
            return 1;
        }

        private Object evaluarLog10(ParseTreeNode der)
        {
            Object val1 = evaluarExp(der);
            int tipo1 = getTipo(val1);

            if (tipo1 == 2 || tipo1 == 1)
                return Math.Log10(Convert.ToDouble(val1));


            this.errores.Append("Función Log10: Tipo de dato inválido, esperaba un valor de tipo númerico. \r\n");
            return 1;
        }

        private Object evaluarAbs(ParseTreeNode der)
        {
            Object val1 = evaluarExp(der);
            int tipo1 = getTipo(val1);

            if (tipo1 == 2 || tipo1 == 1)
                return Math.Abs(Convert.ToDouble(val1));


            this.errores.Append("Funcion Abs: Tipo de dato inválido, esperaba un valor de tipo númerico. \r\n");
            return 1;
        }

        private Object evaluarSin(ParseTreeNode der)
        {
            Object val1 = evaluarExp(der);
            int tipo1 = getTipo(val1);

            if (tipo1 == 2 || tipo1 == 1)
                return Math.Sin(Convert.ToDouble(val1));


            this.errores.Append("Función Sin: Tipo de dato inválido, esperaba un valor de tipo númerico. \r\n");
            return 1;
        }

        private Object evaluarCos(ParseTreeNode der)
        {
            Object val1 = evaluarExp(der);
            int tipo1 = getTipo(val1);

            if (tipo1 == 2 || tipo1 == 1)
                return Math.Cos(Convert.ToDouble(val1));


            this.errores.Append("Función Cos: Tipo de dato inválido, esperaba un valor de tipo númerico. \r\n");
            return 1;
        }

        private Object evaluarTan(ParseTreeNode der)
        {
            Object val1 = evaluarExp(der);
            int tipo1 = getTipo(val1);

            if (tipo1 == 2 || tipo1 == 1)
                return Math.Tan(Convert.ToDouble(val1));


            this.errores.Append("Función Tan: Tipo de dato inválido, esperaba un valor de tipo númerico. \r\n");
            return 1;
        }

        private Object evaluarSqrt(ParseTreeNode der)
        {
            Object val1 = evaluarExp(der);
            int tipo1 = getTipo(val1);

            if (tipo1 == 2 || tipo1 == 1)
                return Math.Sqrt(Convert.ToDouble(val1));


            this.errores.Append("Función Sqrt: Tipo de dato inválido, esperaba un valor de tipo númerico. \r\n");
            return 1;
        }

        private Object evaluarPi()
        {
            return Math.PI;
        }

        private Object evaluarSuma(ParseTreeNode izq, ParseTreeNode der)
        {
            Object val1 = evaluarExp(izq);
            Object val2 = evaluarExp(der);
            int tipo1 = getTipo(val1);
            int tipo2 = getTipo(val2);


            if (tipo1 == 3 || tipo2 == 3 || tipo1 == 4 || tipo2 == 4)
            {
                return val1.ToString() + val2.ToString();
            }

            if (tipo1 == 1)
            {
                if (tipo2 == 1 || tipo2 == 5) return Convert.ToInt32(val1) + Convert.ToInt32(val2);
                else if (tipo2 == 2) return Convert.ToInt32(val1) + Convert.ToDouble(val2);
            }

            if (tipo1 == 2)
            {
                if (tipo2 == 1 || tipo2 == 5) return Convert.ToDouble(val1) + Convert.ToInt32(val2);
                else if (tipo2 == 2) return Convert.ToDouble(val1) + Convert.ToDouble(val2);
            }

            if (tipo1 == 5)
            {
                if (tipo2 == 1) return Convert.ToInt32(val1) + Convert.ToInt32(val2);
                else if (tipo2 == 2) return Convert.ToInt32(val1) + Convert.ToDouble(val2);
                else if (tipo2 == 5) if (Convert.ToBoolean(val1) || Convert.ToBoolean(val2)) return true; else return false;
            }

            this.errores.Append("Suma: Tipo de dato inválido \r\n");
            return 1;
        }

        private Object evaluarResta(ParseTreeNode izq, ParseTreeNode der)
        {
            Object val1 = evaluarExp(izq);
            Object val2 = evaluarExp(der);
            int tipo1 = getTipo(val1);
            int tipo2 = getTipo(val2);


            if (tipo1 == 1)
            {
                if (tipo2 == 1 || tipo2 == 5) return Convert.ToInt32(val1) - Convert.ToInt32(val2);
                else if (tipo2 == 2) return Convert.ToInt32(val1) - Convert.ToDouble(val2);
            }

            if (tipo1 == 2)
            {
                if (tipo2 == 1 || tipo2 == 5) return Convert.ToDouble(val1) - Convert.ToInt32(val2);
                else if (tipo2 == 2) return Convert.ToDouble(val1) - Convert.ToDouble(val2);
            }

            if (tipo1 == 5)
            {
                if (tipo2 == 1) return Convert.ToInt32(val1) - Convert.ToInt32(val2);
                else if (tipo2 == 2) return Convert.ToInt32(val1) - Convert.ToDouble(val2);
            }

            this.errores.Append("Resta: Tipo de dato inválido \r\n");
            return 1;
        }

        private Object evaluarPor(ParseTreeNode izq, ParseTreeNode der)
        {
            Object val1 = evaluarExp(izq);
            Object val2 = evaluarExp(der);
            int tipo1 = getTipo(val1);
            int tipo2 = getTipo(val2);

            if (tipo1 == 1)
            {
                if (tipo2 == 1 || tipo2 == 5) return Convert.ToInt32(val1) * Convert.ToInt32(val2);
                else if (tipo2 == 2) return Convert.ToInt32(val1) * Convert.ToDouble(val2);
            }

            if (tipo1 == 2)
            {
                if (tipo2 == 1 || tipo2 == 5) return Convert.ToDouble(val1) * Convert.ToInt32(val2);
                else if (tipo2 == 2) return Convert.ToDouble(val1) * Convert.ToDouble(val2);
            }

            if (tipo1 == 5)
            {
                if (tipo2 == 1) return Convert.ToInt32(val1) * Convert.ToInt32(val2);
                else if (tipo2 == 2) return Convert.ToInt32(val1) * Convert.ToDouble(val2);
                else if (tipo2 == 5) if (Convert.ToBoolean(val1) && Convert.ToBoolean(val2)) return true; else return false;
            }

            this.errores.Append("Mult: Tipo de dato inválido, esperaba un valor de tipo númerico. \r\n");
            return 1;
        }

        private Object evaluarDividir(ParseTreeNode izq, ParseTreeNode der)
        {
            Object val1 = evaluarExp(izq);
            Object val2 = evaluarExp(der);
            int tipo1 = getTipo(val1);
            int tipo2 = getTipo(val2);

            if (tipo1 == 1)
            {
                if (tipo2 == 1 || tipo2 == 5) return Convert.ToInt32(val1) / Convert.ToInt32(val2);
                else if (tipo2 == 2) return Convert.ToInt32(val1) / Convert.ToDouble(val2);
            }

            if (tipo1 == 2)
            {
                if (tipo2 == 1 || tipo2 == 5) return Convert.ToDouble(val1) / Convert.ToInt32(val2);
                else if (tipo2 == 2) return Convert.ToDouble(val1) / Convert.ToDouble(val2);
            }

            if (tipo1 == 5)
            {
                if (tipo2 == 1) return Convert.ToInt32(val1) / Convert.ToInt32(val2);
                else if (tipo2 == 2) return Convert.ToInt32(val1) / Convert.ToDouble(val2);

            }

            this.errores.Append("División: Tipo de dato inválido, esperaba un valor de tipo númerico. \r\n");
            return 1;
        }

        private Object evaluarPot(ParseTreeNode izq, ParseTreeNode der)
        {
            Object val1 = evaluarExp(izq);
            Object val2 = evaluarExp(der);
            int tipo1 = getTipo(val1);
            int tipo2 = getTipo(val2);

            if (tipo1 == 1 || tipo1 == 2)
            {
                if (tipo2 == 1 || tipo2 == 2) return Math.Pow(Convert.ToDouble(val1), Convert.ToDouble(val2));
            }

            if (tipo1 == 5)
            {
                if (tipo2 == 1 || tipo2 == 2) return Math.Pow(Convert.ToDouble(val1), Convert.ToDouble(val2));
            }

            this.errores.Append("Potencia: Tipo de dato inválido, esperaba un valor de tipo númerico. \r\n");
            return 1;

        }

        private Object evaluarMod(ParseTreeNode izq, ParseTreeNode der)
        {
            Object val1 = evaluarExp(izq);
            Object val2 = evaluarExp(der);
            int tipo1 = getTipo(val1);
            int tipo2 = getTipo(val2);

            if (tipo1 == 1 || tipo1 == 2)
            {
                if (tipo2 == 1 || tipo2 == 2) return Convert.ToDouble(val1) % Convert.ToDouble(val2);
            }

            this.errores.Append("Mod: Tipo de dato inválido \r\n");
            return 1;
        }

        private Object evaluarOr(ParseTreeNode izq, ParseTreeNode der)
        {
            try
            {
                Boolean val1, val2;
                val1 = Convert.ToBoolean(evaluarExp(izq));
                val2 = Convert.ToBoolean(evaluarExp(der));
                return val1 || val2;
            }
            catch (Exception)
            {
                this.errores.Append("Or: solo recibe booleano como parametro");
                return false;
            }
        }

        private Object evaluarAnd(ParseTreeNode izq, ParseTreeNode der)
        {
            try
            {
                Boolean val1, val2;
                val1 = Convert.ToBoolean(evaluarExp(izq));
                val2 = Convert.ToBoolean(evaluarExp(der));
                return val1 && val2;
            }
            catch (Exception)
            {
                this.errores.Append("&& solo recibe booleano como parametro");
                return false;
            }
        }

        private Object evaluarIgual(ParseTreeNode izq, ParseTreeNode der)
        {
            Object val1 = evaluarExp(izq);
            Object val2 = evaluarExp(der);
            return val1.Equals(val2);
        }

        private Object evaluarDiferente(ParseTreeNode izq, ParseTreeNode der)
        {
            Object val1 = evaluarExp(izq);
            Object val2 = evaluarExp(der);
            return !val1.Equals(val2);
        }

        private Object evaluarMayorIgual(ParseTreeNode izq, ParseTreeNode der)
        {
            Object val1 = evaluarExp(izq);
            Object val2 = evaluarExp(der);
            int tipo1 = getTipo(val1);
            int tipo2 = getTipo(val2);

            if (tipo1 == 1)
            {
                if (tipo2 == 1)
                    return Convert.ToInt32(val1) >= Convert.ToInt32(val2);
                else if (tipo2 == 2)
                    return Convert.ToDouble(val1) >= Convert.ToDouble(val2); ;
            }
            else if (tipo1 == 2)
            {
                if (tipo2 == 1 || tipo2 == 2)
                    return Convert.ToDouble(val1) >= Convert.ToDouble(val2);
            }
            this.errores.Append(">= operadores no comparables: " + tipo1 + " >= " + tipo2);
            return false;
        }

        private Object evaluarMayor(ParseTreeNode izq, ParseTreeNode der)
        {
            Object val1 = evaluarExp(izq);
            Object val2 = evaluarExp(der);
            int tipo1 = getTipo(val1);
            int tipo2 = getTipo(val2);

            if (tipo1 == 1)
            {
                if (tipo2 == 1)
                    return Convert.ToInt32(val1) > Convert.ToInt32(val2);
                else if (tipo2 == 2)
                    return Convert.ToDouble(val1) > Convert.ToDouble(val2); ;
            }
            else if (tipo1 == 2)
            {
                if (tipo2 == 1 || tipo2 == 2)
                    return Convert.ToDouble(val1) > Convert.ToDouble(val2);
            }
            this.errores.Append("> operadores no comparables: " + tipo1 + " > " + tipo2);
            return false;
        }

        private Object evaluarMenorigual(ParseTreeNode izq, ParseTreeNode der)
        {
            Object val1 = evaluarExp(izq);
            Object val2 = evaluarExp(der);
            int tipo1 = getTipo(val1);
            int tipo2 = getTipo(val2);

            if (tipo1 == 1)
            {
                if (tipo2 == 1)
                    return Convert.ToInt32(val1) <= Convert.ToInt32(val2);
                else if (tipo2 == 2)
                    return Convert.ToDouble(val1) <= Convert.ToDouble(val2); ;
            }
            else if (tipo1 == 2)
            {
                if (tipo2 == 1 || tipo2 == 2)
                    return Convert.ToDouble(val1) <= Convert.ToDouble(val2);
            }
            this.errores.Append("<= operadores no comparables: " + tipo1 + " <= " + tipo2);
            return false;
        }

        private Object evaluarMenor(ParseTreeNode izq, ParseTreeNode der)
        {
            Object val1 = evaluarExp(izq);
            Object val2 = evaluarExp(der);
            int tipo1 = getTipo(val1);
            int tipo2 = getTipo(val2);

            if (tipo1 == 1)
            {
                if (tipo2 == 1)
                    return Convert.ToInt32(val1) < Convert.ToInt32(val2);
                else if (tipo2 == 2)
                    return Convert.ToDouble(val1) < Convert.ToDouble(val2); ;
            }
            else if (tipo1 == 2)
            {
                if (tipo2 == 1 || tipo2 == 2)
                    return Convert.ToDouble(val1) < Convert.ToDouble(val2);
            }
            this.errores.Append("< operadores no comparables: " + tipo1 + " < " + tipo2);
            return false;
        }

        private Object evaluarNot(ParseTreeNode der)
        {
            Boolean val;
            if (Boolean.TryParse(evaluarExp(der).ToString(), out val))
                return !val;

            this.errores.Append("! solo recibe booleano como parametro");
            return false;
        }

        private Object evaluarId(ParseTreeNode nodo)
        {
            Variable vari= null;//= getVariable(nodo.Token.Text);
            if (vari == null)
            {
                this.errores.Append("variable " + nodo.Token.Text + " no existe");
                return 1;
            }
            if (vari.valor == null)
            {
                this.errores.Append("variable " + nodo.Token.Text + " no tiene valor");
                return 1;
            }
            return vari.valor;
        }

        private Object evaluarNumero(ParseTreeNode nodo)
        {
            if (nodo.Token.Text.Contains("."))
                return Convert.ToDouble(nodo.Token.Text);
            else
                return Convert.ToInt32(nodo.Token.Text);
        }

        private int getTipo(Object var)
        {
            int tipo = 0;
            if (var.GetType() == Type.GetType("System.Int32")) { tipo = 1; }
            else if (var.GetType() == Type.GetType("System.Double")) { tipo = 2; }
            else if (var.GetType() == Type.GetType("System.String")) { tipo = 3; }
            else if (var.GetType() == Type.GetType("System.Char")) { tipo = 4; }
            else if (var.GetType() == Type.GetType("System.Boolean")) { tipo = 5; }
            else if (var.ToString().ToLower().Equals("falso") || var.ToString().ToLower().Equals("verdadero")) { tipo = 5; }
            return tipo;
        }
    }
}
