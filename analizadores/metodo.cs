﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Irony.Ast;
using Irony.Parsing;

 namespace usacXForm.analizadores
{
    class metodo
    {
        public string nombre;
        public string tipo;
        public string visibilidad;
        public Dictionary<string, string> simbolos;
        public ParseTreeNode sentencia;
        public ParseTreeNode parametros;
        public int noMetodo;

        public metodo() { }

        public metodo(string nombre, string tipo, string visibilidad, Dictionary<string, string> simbolos, ParseTreeNode sentencia, ParseTreeNode parametros, int noMetodo)
        {
            this.nombre = nombre;
            this.tipo = tipo;
            this.visibilidad = visibilidad;
            this.simbolos = simbolos;
            this.sentencia = sentencia;
            this.parametros = parametros;
            this.noMetodo = noMetodo;
        }

        public ParseTreeNode getSentencias()
        {
            return this.sentencia;
        }
    }
}
