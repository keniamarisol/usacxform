﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using usacXForm.TablaSimbolos;

namespace usacXForm.analizadores
{
    class clase
    {
        public string nombre;
        public string visibilidad;
        public Dictionary<string, string> simbolos;
        public Dictionary<string, metodo> metodos;
        public Dictionary<string, metodo> constructores;
        public Dictionary<string, metodo> principal;
        public Dictionary<string, metodo> formulario;
        public Dictionary<string, metodo> pregunta;
        public Dictionary<string, metodo> grupo;
        public ArrayList hereda = new ArrayList();

        public clase()
        {
        }

        public clase(string nombre, string visibilidad, Dictionary<string, string> simbolos, Dictionary<string, metodo> metodos, Dictionary<string, metodo> constructores, ArrayList hereda)
        {
            this.nombre = nombre;
            this.visibilidad = visibilidad;
            this.simbolos = simbolos;
            this.metodos = metodos;
            this.constructores = constructores;
            this.hereda = hereda;
        }


    }
}
