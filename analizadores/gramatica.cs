﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Irony.Ast;
using Irony.Parsing;

namespace usacXForm.analizadores
{
    class gramatica : Grammar
    {
        public gramatica() : base(caseSensitive: false)
        {
            #region ER
            NumberLiteral numero = TerminalFactory.CreateCSharpNumber("numero");
            IdentifierTerminal id = TerminalFactory.CreateCSharpIdentifier("id");
            var tstring = new StringLiteral("tstring", "\"", StringOptions.AllowsDoubledQuote);
            var tchar = new StringLiteral("tchar", "'", StringOptions.AllowsDoubledQuote);
            #endregion

            #region palabrasReservadas
            KeyTerm prCadena = ToTerm("Cadena"),
            prCaracter = ToTerm("Caracter"),
            prBooleano = ToTerm("Booleano"),
            prEntero = ToTerm("Entero"),
            prDecimal = ToTerm("Decimal"),
            prVacio = ToTerm("Vacio"),
            prHora = ToTerm("Hora"),
            prFecha = ToTerm("Fecha"),
            prFechaHora = ToTerm("FechaHora"),
            //  prRespuesta = ToTerm("Respuesta"),
            prPublico = ToTerm("publico"),
            prProtegido = ToTerm("protegido"),
            prPrivado = ToTerm("privado"),
            prClase = ToTerm("clase"),
            prPadre = ToTerm("padre"),
            prSuper = ToTerm("super"),
            prRetorno = ToTerm("Retorno"),
            prPrincipal = ToTerm("Principal"),
            prSi = ToTerm("SI"),
            prSiNo = ToTerm("SINO"),
            prCaso = ToTerm("CASO"),
            prDe = ToTerm("de"),
            prDefecto = ToTerm("Defecto"),
            prRomper = ToTerm("romper"),
            prMientas = ToTerm("Mientras"),
            prContinuar = ToTerm("Continuar"),
            prMientras = ToTerm("Mientras"),
            prHacer = ToTerm("Hacer"),
            prRepetir = ToTerm("Repetir"),
            prHasta = ToTerm("hasta"),
            prPara = ToTerm("Para"),
            prImprimir = ToTerm("Imprimir"),
            prMensajes = ToTerm("Mensajes"),
            prSubcad = ToTerm("subCad"),
            prPoscad = ToTerm("posCad"),
            prTam = ToTerm("Tam"),
            prRandom = ToTerm("random"),
            prMin = ToTerm("min"),
            prMax = ToTerm("max"),
            prPow = ToTerm("pow"),
            prLog = ToTerm("Log"),
            prLog10 = ToTerm("Log10"),
            prAbs = ToTerm("Abs"),
            prSen = ToTerm("Sin"),
            prCos = ToTerm("Cos"),
            prTan = ToTerm("tan"),
            prSqrt = ToTerm("Sqrt"),
            prPi = ToTerm("Pi"),
            prHoy = ToTerm("hoy()"),
            prAhora = ToTerm("Ahora()"),
            prImagen = ToTerm("imagen"),
            prVideo = ToTerm("video"),
            prAudio = ToTerm("audio"),
            prVerdadero = ToTerm("verdadero"),
            prFalso = ToTerm("falso"),
            prOpciones = ToTerm("Opciones"),
            prNuevo = ToTerm("Nuevo"),
            prInsertar = ToTerm("insertar"),
            prObtener = ToTerm("obtener"),
            prBuscar = ToTerm("buscar"),
            prImportar = ToTerm("importar"),
            prPregunta = ToTerm("pregunta"),
            prFormulario = ToTerm("formulario"),
            prGrupo = ToTerm("grupo"),
            presCadena = ToTerm("esCadena"),
            presCaracter = ToTerm("esCaracter"),
            presBooleano = ToTerm("esBooleano"),
            presEntero = ToTerm("esEntero"),
            presDecimal = ToTerm("esDecimal"),
            presVacio = ToTerm("esVacio"),
            presHora = ToTerm("esHora"),
            presFecha = ToTerm("esFecha"),
            presFechaHora = ToTerm("esFechaHora");

            MarkReservedWords("Cadena", "Caracter", "Booleano", "Entero", "Decimal", "Vacio", "Hora", "Fecha", "FechaHora", "publico", "protegido", "privado", "clase",
           "padre", "super", "Retorno", "Principal");

            /*"SI", "SINO", "CASO", "de", "Defecto", "romper", "Mientras", "Continuar", "Mientras", "Repetir", "hasta",
            "Para", "Imprimir", "Mensajes", "subCad", "posCad", "Tam", "random", "min", "max", "pow", "Log", "Log10", "Abs", "Sin", "Cos", "tan", "Sqrt",
            "Pi", "hoy()", "Ahora()", "imagen", "video", "audio", "verdadero", "falso", "Opciones", "Nuevo", "insertar", "obtener", "buscar", "importar", "opciones",
           "pregunta", "formulario"*/
            #endregion

            #region Comentarios
            CommentTerminal comentarioMultilinea = new CommentTerminal("comentarioMultiLinea", "$#", "#$");
            base.NonGrammarTerminals.Add(comentarioMultilinea);
            CommentTerminal comentarioUnilinea = new CommentTerminal("comentarioUniLinea", "$$", "\n", "\r\n");
            base.NonGrammarTerminals.Add(comentarioUnilinea);
            #endregion

            #region Operadores y simbolos
            Terminal ptcoma = ToTerm(";"),
                coma = ToTerm(","),
                punto = ToTerm("."),
                dospuntos = ToTerm(":"),
                parizq = ToTerm("("),
                parder = ToTerm(")"),
                llaizq = ToTerm("{"),
                llader = ToTerm("}"),
                signoMas = ToTerm("+"),
                signoMenos = ToTerm("-"),
                adicion = ToTerm("++"),
                sustraccion = ToTerm("--"),
                signoPor = ToTerm("*"),
                signoDiv = ToTerm("/"),
                signoPot = ToTerm("^"),
                signoMod = ToTerm("%"),
                igualQue = ToTerm("=="),
                diferenteQue = ToTerm("!="),
                menorQue = ToTerm("<"),
                mayorIgualQue = ToTerm(">="),
                menorIgualQue = ToTerm("<="),
                mayorQue = ToTerm(">"),
                or = ToTerm("||"),
                and = ToTerm("&&"),
                not = ToTerm("!"),
                pr_true = ToTerm("true", "true"),
                pr_false = ToTerm("false", "false"),
                igual = ToTerm("="),
                interrogacion = ToTerm("?"),
                corizq = ToTerm("["),
                corder = ToTerm("]");
            #endregion

            #region No terminales
            NonTerminal S = new NonTerminal("S"),
             E = new NonTerminal("E"),
             F = new NonTerminal("F"),
             T = new NonTerminal("T"),
             TIPO = new NonTerminal("TIPO"),
             EXP = new NonTerminal("EXP"),
             OPMAT = new NonTerminal("OPMAT"),
             SENTENCIAS = new NonTerminal("SENTENCIAS"),
             ASIGNACION = new NonTerminal("ASIGNACION"),
             VISIBILIDAD = new NonTerminal("VISIBILIDAD");
            #endregion

            var PROGRAMA = new NonTerminal("PROGRAMA");
            var CUERPO = new NonTerminal("CUERPO");
            var SENTENCIA = new NonTerminal("SENTENCIA");
            var DECLARAR = new NonTerminal("DECLARAR");
            var DECLARAR_ASIG = new NonTerminal("DECLARAR_ASIG");
            var DECLARAR_ARRAY = new NonTerminal("DECLARAR_ARRAY");
            var ARRAY = new NonTerminal("ARRAY");
            var L_ARRAY = new NonTerminal("L_ARRAY");
            var CLASE = new NonTerminal("CLASE");
            var IMPRIMIR = new NonTerminal("IMPRIMIR");
            var RETORNO = new NonTerminal("RETORNO");

            var ID_LISTA = new NonTerminal("ID_LISTA");
            var LISTA_CAS = new NonTerminal("LISTA_CAS");
            var CASILLA = new NonTerminal("CASILLA");
            var SENT = new NonTerminal("SENT");
            var SI = new NonTerminal("SI");
            var SISINO_SIMP = new NonTerminal("SISINO_SIMP");
            var SINOSI = new NonTerminal("SINOSI");
            var SINO = new NonTerminal("SINO");
            var SINOSI_LIST = new NonTerminal("SINOSI_LIST");
            var MIENTRAS = new NonTerminal("MIENTRAS");
            var HACER = new NonTerminal("HACER");
            var REPETIR = new NonTerminal("REPETIR");
            var PARA = new NonTerminal("PARA");
            var LOOP = new NonTerminal("LOOP");
            var COUNT = new NonTerminal("COUNT");
            var DOWHILEX = new NonTerminal("DOWHILEX");
            var WHILEX = new NonTerminal("WHILEX");
            var METODO = new NonTerminal("METODO");
            var PARAMETRO_LIST = new NonTerminal("PARAMETRO_LIST");
            var PARAMETRO = new NonTerminal("PARAMETRO");
            var PRINCIPAL = new NonTerminal("MAIN");
            var ACCESO_OBJ = new NonTerminal("ACCESO_OBJ");
            var LISTA_ACCESO = new NonTerminal("LISTA_ACCESO");
            var ACCESO = new NonTerminal("ACCESO");
            var CALLFUN = new NonTerminal("CALLFUN");
            var PARAMETROS2_LIST = new NonTerminal("PARAMETROS2_LIST");
            var OBJETO_ASIGNACION = new NonTerminal("ASIGNACION_OBJECTO");
            var ROMPER = new NonTerminal("ROMPER");
            var CONTINUAR = new NonTerminal("CONTINUAR");
            var CASE = new NonTerminal("CASE");
            var CASO = new NonTerminal("CASO");
            var CASOS = new NonTerminal("CASOS");
            var DEFAULT = new NonTerminal("DEFAULT");
            var VAL_CASE = new NonTerminal("VALCASE");
            var LISTA_COR = new NonTerminal("LISTA_COR");
            var CORCHETES = new NonTerminal("CORCHETES");

            var CLASE_SENTENCIAS = new NonTerminal("CLASE_SENTENCIAS");
            var CLASE_SENT = new NonTerminal("CLASE_SENT");
            var TIPO_M = new NonTerminal("TIPO_METODO");
            var IMPORTAR = new NonTerminal("IMPORTAR");
            var CONSTRUCTOR = new NonTerminal("CONSTRUCTOR");
            var DECREMENTOS = new NonTerminal("DECREMENTOS");
            var OVERRIDE = new NonTerminal("OVERRIDE");
            var INSTANCIA = new NonTerminal("INSTANCIA");
            var ASIGNAR_INSTANCE = new NonTerminal("ASIG_INSTANCIA");
            var PARA_COND = new NonTerminal("PARA_COND");
            var LLAVE = new NonTerminal("LLAVE");
            var ACCESO_ARRAY = new NonTerminal("ACCESO_ARRAY");
            var ASIG_ARRAY = new NonTerminal("ASIG_ARRAY");
            var ESTE = new NonTerminal("ESTE");
            var LLAMAR = new NonTerminal("LLAMAR");
            var AUXILIAR_INSTANCIA = new NonTerminal("instance");
            var SENTENCIA_GLOBAL = new NonTerminal("SENTENCIA_GLOBAL");
            var HEREDA = new NonTerminal("HEREDA");
            var ESTE_SENTENCIAS = new NonTerminal("SENT_THIS");
            var AUXILIAR = new NonTerminal("AUXILIAR");
            var SUPER = new NonTerminal("SUPER");
            var ID = new NonTerminal("ID");
            var OPCIONES = new NonTerminal("OPCIONES");
            var ACCESO_ELEMENTO = new NonTerminal("ACCESO_ELEMENTO");
            var EXP_LISTA = new NonTerminal("EXP_LISTA");
            var FORMULARIO = new NonTerminal("FORMULARIO");
            var PREGUNTA = new NonTerminal("PREGUNTA");
            var GRUPO = new NonTerminal("GRUPO");
            var RESPUESTA_METODO = new NonTerminal("RESPUESTA_METODO");
            var CALCULAR_METODO = new NonTerminal("CALCULAR_METODO");
            var MOSTRAR_METODO = new NonTerminal("MOSTRAR_METODO");
            var FORM_LLAMADA = new NonTerminal("FORM_LLAMADA");
            var GRUPO_LLAMADA = new NonTerminal("GRUPO_LLAMADA");
            var PREGUNTA_LLAMADA = new NonTerminal("PREGUNTA_LLAMADA");
            var RESPUESTA_PROC = new NonTerminal("LLAMADA_FORMULARIO");
            var TIPO_ATRIBUTO_PREG = new NonTerminal("TIPO_ATRIBUTO_PREG");
            var PREG_CONTENIDO = new NonTerminal("PREG_CONTENIDO");
            var FORM_CONTENIDO = new NonTerminal("FORM_CONTENIDO");
            var GRUPO_CONTENIDO = new NonTerminal("GRUPO_CONTENIDO");
            var GRUPO_CONTENIDO_LISTA = new NonTerminal("GRUPO_CONTENIDO_LISTA");
            var PREG_CONTENIDO_LISTA = new NonTerminal("PREG_CONTENIDO_LISTA");
            var ES_TIPO = new NonTerminal("ES_TIPO");
            var DECLARAR_GLOBAL = new NonTerminal("DECLARAR_GLOBAL");
            var DECLARAR_ASIG_GLOBAL = new NonTerminal("DECLARAR_ASIG_GLOBAL");
            var INSTANCIAR_GLOBAL = new NonTerminal("INSTANCIAR_GLOBAL");
            var ARREGLO_GLOBAL = new NonTerminal("ARREGLO_GLOBAL");

            #region inicio
            PROGRAMA.Rule = MakeStarRule(PROGRAMA, CUERPO);
            #endregion fin inicio

            #region clase
            CUERPO.Rule = IMPORTAR | CLASE | LLAMAR;

            IMPORTAR.Rule = prImportar + parizq + ID + punto + ID + parder + ptcoma;

            LLAMAR.Rule = prSuper + parizq + tstring + parder + ptcoma;

            CLASE.Rule = prClase + ID + VISIBILIDAD + HEREDA + llaizq + CLASE_SENTENCIAS + llader;

            HEREDA.Rule = prPadre + ID
           | Empty;

            PRINCIPAL.Rule = prPrincipal + parizq + parder + llaizq + SENTENCIAS + llader;

            CLASE_SENTENCIAS.Rule = MakeStarRule(CLASE_SENTENCIAS, CLASE_SENT);

            VISIBILIDAD.Rule = prPublico
            | prProtegido
            | prPrivado
            | Empty;


            CLASE_SENT.Rule =  CONSTRUCTOR
                             | DECLARAR_GLOBAL
                             | INSTANCIAR_GLOBAL
                             | DECLARAR_ASIG_GLOBAL
                             | ARREGLO_GLOBAL
                             | OVERRIDE
                             | PRINCIPAL
                             | METODO
                             | FORMULARIO
                             | PREGUNTA
                             | GRUPO
                             ;

            DECLARAR_GLOBAL.Rule = TIPO + VISIBILIDAD + ID_LISTA + ptcoma; //variable
            INSTANCIAR_GLOBAL.Rule = TIPO + VISIBILIDAD + ID + igual + prNuevo + ID + AUXILIAR_INSTANCIA + ptcoma; //instanciar
            DECLARAR_ASIG_GLOBAL.Rule = TIPO + VISIBILIDAD + ID + igual + EXP + ptcoma; //declarar y asignar
            ARREGLO_GLOBAL.Rule = TIPO + VISIBILIDAD + ID + L_ARRAY + igual + LLAVE + ptcoma; //arreglo
                                
            METODO.Rule = VISIBILIDAD + TIPO_M + ID + parizq + parder + llaizq + SENTENCIAS + llader
                         | VISIBILIDAD + TIPO_M + ID + parizq + PARAMETRO_LIST + parder + llaizq + SENTENCIAS + llader;

            TIPO_M.Rule = TIPO//PRODUCCION PARA DECLARAR EL TIPO DE LOS METODOS
                         | prVacio
                         ;

            OVERRIDE.Rule = ToTerm("@sobrescribir") + METODO;

            CONSTRUCTOR.Rule = VISIBILIDAD + ID + parizq + parder + llaizq + SENTENCIAS + llader
                             | VISIBILIDAD + ID + parizq + PARAMETRO_LIST + parder + llaizq + SENTENCIAS + llader;
            #endregion clase

            #region sentencias
            SENTENCIAS.Rule = MakeStarRule(SENTENCIAS, SENTENCIA);

            SENTENCIA.Rule = FORM_LLAMADA + ptcoma
                             | PREGUNTA_LLAMADA + ptcoma
                             | ASIGNAR_INSTANCE + ptcoma
                             | INSTANCIA + ptcoma
                             | ASIGNACION + ptcoma
                             | DECLARAR_ASIG + ptcoma
                             | DECLARAR_ARRAY + ptcoma
                             | DECLARAR + ptcoma
                             | ASIG_ARRAY + ptcoma
                             | SI
                             | SISINO_SIMP
                             | IMPRIMIR + ptcoma
                             | MIENTRAS
                             | HACER
                             | REPETIR
                             | PARA
                             | RETORNO + ptcoma
                             | CALLFUN + ptcoma
                             | ACCESO_OBJ + ptcoma
                             | ROMPER + ptcoma
                             | CONTINUAR + ptcoma
                             | OBJETO_ASIGNACION + ptcoma
                             | DECREMENTOS + ptcoma
                             | ESTE
                             | SyntaxError;


            ESTE_SENTENCIAS.Rule = ASIGNAR_INSTANCE
                                 | OBJETO_ASIGNACION
                                 | ASIGNACION
                                 | ASIG_ARRAY
                                 | CALLFUN;

            DECLARAR.Rule = TIPO + ID_LISTA;

            ID_LISTA.Rule = MakeStarRule(ID_LISTA, ToTerm(","), ID);

            ID.Rule = id;


            INSTANCIA.Rule = TIPO + ID + igual + prNuevo + ID + AUXILIAR_INSTANCIA;

            ASIGNAR_INSTANCE.Rule = ID + igual + prNuevo + ID + AUXILIAR_INSTANCIA;

            AUXILIAR_INSTANCIA.Rule = parizq + parder
                                     | parizq + PARAMETROS2_LIST + parder;

            ESTE.Rule = ToTerm("este") + punto + ESTE_SENTENCIAS + ptcoma;

            ASIGNACION.Rule = ID + igual + EXP
                            | ID + igual + ACCESO_OBJ;

            DECLARAR_ASIG.Rule = TIPO + ID + igual + EXP
                             | TIPO + ID + igual + ACCESO_OBJ;

            //******************************************************************************
            ACCESO_OBJ.Rule = TIPO + punto + LISTA_ACCESO
                           | CALLFUN + punto + LISTA_ACCESO
                           ;

            LISTA_ACCESO.Rule = MakeListRule(LISTA_ACCESO, ToTerm("."), ACCESO);

            OBJETO_ASIGNACION.Rule = ACCESO_OBJ + igual + EXP
                              | ACCESO_OBJ + igual + ACCESO_OBJ;

            AUXILIAR.Rule = EXP
                         | ACCESO_OBJ;

            ACCESO.Rule = ID
                        | CALLFUN;


            //*******************************************

            DECREMENTOS.Rule = ID + "++"
                            | ID + "--";

            DECLARAR_ARRAY.Rule = TIPO + ID + L_ARRAY + igual + LLAVE
                                 | TIPO + ID + L_ARRAY
                                 ;

            L_ARRAY.Rule = MakeStarRule(L_ARRAY, CASILLA);

            CASILLA.Rule = corizq + EXP + corder;

            LLAVE.Rule = llaizq + PARAMETROS2_LIST + llader;

            ACCESO_ARRAY.Rule = ID + L_ARRAY;

            ASIG_ARRAY.Rule = ID + L_ARRAY + igual + EXP;

            IMPRIMIR.Rule = prImprimir + parizq + EXP + parder;

            SI.Rule = prSi + parizq + EXP + parder + llaizq + SENTENCIAS + llader + SINO
                    | prSi + parizq + EXP + parder + llaizq + SENTENCIAS + llader + SINOSI_LIST + SINO
                    ;

            SINOSI_LIST.Rule = MakeStarRule(SINOSI_LIST, SINOSI);

            SINOSI.Rule = prSiNo + prSi + parizq + EXP + parder + llaizq + SENTENCIAS + llader;

            SINO.Rule = prSiNo + llaizq + SENTENCIAS + llader
                      | Empty;


            SISINO_SIMP.Rule = EXP + ToTerm("?") + EXP + ToTerm(":") + EXP;

            MIENTRAS.Rule = prMientas + parizq + EXP + parder + llaizq + SENTENCIAS + llader;

            HACER.Rule = prHacer + llaizq + SENTENCIAS + llader + prMientas + parizq + EXP + parder + ptcoma;

            REPETIR.Rule = prRepetir + llaizq + SENTENCIAS + llader + prHasta + parizq + EXP + parder + ptcoma;


            PARA.Rule = prPara + parizq + PARA_COND + ptcoma + EXP + ptcoma + DECREMENTOS + parder + llaizq + SENTENCIAS + llader;

            PARA_COND.Rule = ASIGNACION
                         | DECLARAR_ASIG;

            RETORNO.Rule = prRetorno + EXP
                         | prRetorno;

            ROMPER.Rule = prRomper;

            CONTINUAR.Rule = prContinuar;

            PARAMETRO_LIST.Rule = MakeStarRule(PARAMETRO_LIST, coma, PARAMETRO);

            PARAMETRO.Rule = TIPO + ID;

            PARAMETROS2_LIST.Rule = MakeStarRule(PARAMETROS2_LIST, coma, EXP);

            CALLFUN.Rule = ID + parizq + parder
             | ID + parizq + PARAMETROS2_LIST + parder;



            TIPO.Rule = prVacio
           | prEntero
           | prDecimal
           | prCadena
           | prCaracter
           | prBooleano
           | prFecha
           | prHora
           | prFechaHora
           //   | prRespuesta
           | ID;

            EXP.Rule = EXP + or + EXP
                | EXP + and + EXP
                | EXP + igualQue + EXP
                | EXP + diferenteQue + EXP
                | EXP + mayorQue + EXP
                | EXP + menorQue + EXP
                | EXP + mayorIgualQue + EXP
                | EXP + menorIgualQue + EXP
                | EXP + signoMas + EXP
                | EXP + signoMenos + EXP
                | signoMas + EXP
                | signoMenos + EXP
                | EXP + signoMas + signoMas
                | EXP + signoMenos + signoMenos
                | EXP + signoPor + EXP
                | EXP + signoDiv + EXP
                | EXP + signoPot + EXP
                | EXP + signoMod + EXP
                | parder + EXP + parizq
                | not + EXP
                | prLog + parder + EXP + parizq
                | prLog10 + parder + EXP + parizq
                | prAbs + parder + EXP + parizq
                | prSen + parder + EXP + parizq
                | prCos + parder + EXP + parizq
                | prTan + parder + EXP + parizq
                | prSqrt + parder + EXP + parizq
                | prPow + parder + EXP + coma + EXP + parizq
                | prPi + parder + parizq
                | ID
                | numero
                | tstring
                | tchar
                | prFalso
                | prVerdadero
                | CALLFUN
                | ACCESO_OBJ;

            #endregion sentencias


            #region preguntas
            OPCIONES.Rule = prOpciones + id + igual + prNuevo + prOpciones + parizq + parder;

            ACCESO_ELEMENTO.Rule = ID + punto + prInsertar + parizq + EXP_LISTA + parder;

            EXP_LISTA.Rule = MakeStarRule(EXP_LISTA, coma, EXP);

            FORMULARIO.Rule = prFormulario + ID + llaizq + SENTENCIAS + llader;

            GRUPO.Rule = prGrupo + ID + llaizq + GRUPO_CONTENIDO_LISTA + llader
            | prPregunta + ID + parizq + PARAMETRO_LIST + parder + llaizq + GRUPO_CONTENIDO_LISTA + llader;

            PREGUNTA.Rule = prPregunta + ID + parizq + parder + llaizq + PREG_CONTENIDO_LISTA + llader
                         | prPregunta + ID + parizq + PARAMETRO_LIST + parder + llaizq + PREG_CONTENIDO_LISTA + llader;


            RESPUESTA_METODO.Rule = prPublico + ToTerm("respuesta") + parizq + parder + llaizq + SENTENCIAS + llader
                         | prPublico + ToTerm("respuesta") + parizq + PARAMETRO_LIST + parder + llaizq + SENTENCIAS + llader;

            CALCULAR_METODO.Rule = prPublico + ToTerm("@calcular") + parizq + parder + llaizq + SENTENCIAS + llader;

            MOSTRAR_METODO.Rule = prPublico + ToTerm("@mostrar") + parizq + parder + llaizq + SENTENCIAS + llader; ;

            FORM_LLAMADA.Rule = prNuevo + ACCESO_OBJ;
            ;

            PREG_CONTENIDO_LISTA.Rule = MakeStarRule(PREG_CONTENIDO_LISTA, PREG_CONTENIDO);

            PREG_CONTENIDO.Rule = ASIGNACION + ptcoma
                | DECLARAR_ASIG + ptcoma
                | DECLARAR + ptcoma
                | RESPUESTA_METODO
                | CALCULAR_METODO
                | MOSTRAR_METODO;

            PREGUNTA_LLAMADA.Rule = ID + ToTerm("(") + ToTerm(")") + ToTerm(".") + ToTerm("respuesta") + ToTerm("(") + ToTerm("resp") + punto + ES_TIPO + ToTerm(")") + punto + TIPO + parizq + PARAMETROS2_LIST + parder
                                  | ID + ToTerm("(") + ToTerm(")") + ToTerm(".") + ToTerm("respuesta") + ToTerm("(") + ToTerm("resp") + punto + ES_TIPO + ToTerm(")") + punto + TIPO + parizq + parder
                                  | ID + ToTerm("(") + ToTerm(")") + punto + ToTerm("nota") + parizq + parder;

            ES_TIPO.Rule = presCadena | presCaracter | presFecha | presFechaHora | presHora | presBooleano
                           | presEntero | presDecimal;

            GRUPO_CONTENIDO_LISTA.Rule = MakeStarRule(GRUPO_CONTENIDO_LISTA, GRUPO_CONTENIDO);

            GRUPO_CONTENIDO.Rule = PREGUNTA_LLAMADA + ptcoma
                                  | ASIGNACION + ptcoma
                                  | DECLARAR_ASIG + ptcoma
                                  | DECLARAR + ptcoma
                                  | ASIGNAR_INSTANCE + ptcoma
                                  | INSTANCIA + ptcoma
                                  | SI
                                  | PARA;

            #endregion preguntas

            SENTENCIA.ErrorRule = SyntaxError + SENTENCIA;
            CUERPO.ErrorRule = SyntaxError + CUERPO;

            #region Preferencias
            this.Root = PROGRAMA;
            this.MarkTransient( CLASE_SENT, CUERPO, SENTENCIA, PARA_COND, ARRAY,
                ACCESO, CASILLA, GRUPO_CONTENIDO, PARAMETRO);

            //---------------------> Eliminacion de caracters, no terminales
            this.MarkPunctuation("(", ")", ";", ":", "{", "}", "=", ".", ",", "[", "]", "@sobrescribir");
            this.MarkPunctuation("repetir", "until", "imprimir", "principal", "x", "retornar", "este");
            this.MarkPunctuation("Si", "Sino", "Sino Si", "Mientras", "Hacer", "para", "llamar", "clase", "hereda_de", "importar");
            #endregion


            #region precedencia
            /*
             * En esta region se define la precedencia y asociatividad para remover la ambiguedad
             * de la gramatica de expresiones.             
            */
            RegisterOperators(1, Associativity.Right, igual);
            RegisterOperators(2, Associativity.Left, or);
            RegisterOperators(4, Associativity.Left, and);
            RegisterOperators(5, Associativity.Left, igualQue, diferenteQue);
            RegisterOperators(6, Associativity.Left, mayorQue, menorQue, mayorIgualQue, menorIgualQue);
            RegisterOperators(7, Associativity.Left, signoMas, signoMenos);
            RegisterOperators(8, Associativity.Left, signoPor, signoDiv);
            RegisterOperators(9, Associativity.Right, signoPot);
            RegisterOperators(10, Associativity.Right, not, adicion, sustraccion);
            RegisterOperators(11, Associativity.Left, punto);
            RegisterOperators(12, Associativity.Neutral, parder, parizq);
            #endregion

        }
    }
}
