﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Irony.Ast;
using Irony.Parsing;
using usacXForm.graficar;
using System.Drawing;


namespace usacXForm.analizadores
{
    class sintactico 
    {

        public static bool analizar(String cadena) {
            gramatica gram = new gramatica();
            LanguageData lenguaje = new LanguageData(gram);
            Parser parser = new Parser(lenguaje);
            ParseTree arbol = parser.Parse(cadena);
            ParseTreeNode raiz = arbol.Root;

            if (raiz == null)
            {
                return false;
            }

            interprete interp = new interprete();
            interp.ejecutar(raiz);
            generarImagen(raiz);
            return true;
        }

        private static void generarImagen(ParseTreeNode raiz) {
            String graf = graficar.graficar.getdot(raiz);
            WINGRAPHVIZLib.DOT dot = new WINGRAPHVIZLib.DOT();
            WINGRAPHVIZLib.BinaryImage img = dot.ToPNG(graf);

            img.Save("AST.png");
        }
    }
}
