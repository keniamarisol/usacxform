﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
// 1. Use Easy Tabs
using EasyTabs;

namespace usacXForm
{
    public partial class tabContent : Form
    {
        public tabContent()
        {
            InitializeComponent();
        }


        // 2. Important: Declare ParentTabs
        protected TitleBarTabs ParentTabs
        {
            get
            {
                return (ParentForm as TitleBarTabs);
            }
        }

    }
}

